
// @GENERATOR:play-routes-compiler
// @SOURCE:/home/elias/Desarrollo/ecalcula-root-api/conf/routes
// @DATE:Sat Aug 19 19:23:51 PYT 2017


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
