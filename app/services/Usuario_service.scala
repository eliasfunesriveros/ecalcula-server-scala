package services

import javax.inject.{Inject, Singleton}

import Models.Usuario
import play.api.libs.json.JsValue
import play.api.db.Database


@Singleton
class Usuario_service @Inject()(db:Database) {

   def find_usuario(request_body : JsValue): JsValue ={
     val sql =  "select * from func_get_usuario(?, ?, ?)"

     val conn = db.getConnection()
     val statement = conn.prepareStatement(sql)
     statement.setString(1, request_body("auth0id").as[String])
     statement.setString(2, request_body("email").as[String])
     statement.setString(3, request_body("nickname").as[String])
     val rs = statement.executeQuery()

     val usuario = new Usuario()
     while(rs.next()){
       usuario.set_id(rs.getInt("id"))
       usuario.set_auth0_id(rs.getString("auth0_id"))
       usuario.set_email(rs.getString("email"))
       usuario.set_nickname(rs.getString("nickname"))
       usuario.set_tipo_usuario(rs.getString("tipo_usuario"))
       usuario.set_habilitado(rs.getBoolean("habilitado"))
     }

     conn.close();

     usuario.toJsonObj()
   }



}
