package controllers




import javax.inject.{Inject, _}

import play.api.mvc._
import services.Usuario_service

@Singleton
class Application @Inject()
  (
    cc: ControllerComponents,
    usuario_service: Usuario_service
  ) extends AbstractController(cc){


  def testPost = Action { request => Ok(usuario_service.find_usuario(request.body.asJson.get))}


}
