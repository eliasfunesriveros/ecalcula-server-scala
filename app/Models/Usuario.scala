package Models

import play.api.libs.json._

class Usuario {

  private var id:Integer = _
  private var auth0_id:String = _
  private var email:String = _
  private var nickname:String = _
  private var tipo_usuario:String = _
  private var habilitado:Boolean = _



  //SETTERS
  def set_id(id:Integer): Unit ={
    this.id = id
  }

  def set_auth0_id(auth0_id:String): Unit ={
    this.auth0_id = auth0_id
  }

  def set_email(email:String): Unit ={
    this.email = email
  }

  def set_nickname(nickname:String): Unit ={
    this.nickname = nickname
  }

  def set_tipo_usuario(tipo_usuario:String): Unit ={
    this.tipo_usuario = tipo_usuario
  }

  def set_habilitado(habilitado:Boolean): Unit ={
    this.habilitado = habilitado
  }



  //GETTERS
  def get_id():Integer = {
    return this.id
  }

  def get_auth0_id():String = {
    return this.auth0_id
  }

  def get_email():String = {
    return this.email
  }

  def get_nickname():String = {
    return this.nickname
  }

  def get_tipo_usuario():String = {
    return this.tipo_usuario
  }

  def get_habilitado():Boolean = {
    return this.habilitado
  }

  def toJsonObj():JsValue = {
     val json : JsValue = Json.obj(
       "id" -> id.toInt,
       "auth0_id" -> auth0_id,
       "email" -> email,
       "nickname" -> nickname,
       "tipo_usuario" -> tipo_usuario,
       "habilitado" -> habilitado
     )
    return json

  }

}

